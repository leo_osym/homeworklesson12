﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace MyMenu
{
    delegate void Items(string str);
    class MenuItem
    {
        public MenuItem(string name, double price)
        {
            Name = name;
            Price = price;
        }
        public delegate void Item(double num);
        public static event Item MyEvent;
        public string Name{ get; private set;}
        public double Price { get; private set; }
        public override string ToString()
        {
            return Name;
        }
        public void EvaluateMyCar(double price)
        {
            MyEvent(price);
        }

    }
    class Handler
    {
        public void goodChoice(double price)
        {
            if(price < 20000)
            {
                Console.WriteLine("Oh, {0,5:c0} is quite low price for our market! ", price);
            }
            else if (price < 100000)
            {
                Console.WriteLine("{0,5:c0} is enough price for a good car!",price);
            }
            else
            {
                Console.WriteLine("Wow! This is a very expencive car! {0,5:c0}", price);
            }
        }
    }
    class MyMenu: IEnumerable
    {
        MenuItem[] menu = new MenuItem[6];
        public MyMenu()
        {
            menu[0] = new MenuItem("Honda Civic", 10000);
            menu[1] = new MenuItem("BMW i3",30000);
            menu[2] = new MenuItem("Lamborgini Galardo",110000);
            menu[3] = new MenuItem("Toyota RAV4",40000);
            menu[4] = new MenuItem("Ford Fiesta",15000);
            menu[5] = new MenuItem("Volkswagen Golf",12000);
            Count = menu.Length;
            Handler handler = new Handler();
            MenuItem.MyEvent += handler.goodChoice;
        }
        public int Count { get; private set; }
        public MenuItem this[int index]
        {
            get => menu[index];
            set => menu[index] = value;
        }

        public IEnumerator GetEnumerator()
        {
            foreach(var item in menu)
            {
                yield return item;
            }
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            MyMenu menu = new MyMenu();
            Console.WriteLine("Please, enter index of the car to see some info");
            Console.WriteLine("----INDEX---------CAR----------PRICE----");
            for(int i = 0; i<menu.Count; i++)
            {
                Console.WriteLine("{0,5} {1,20} {2,10:c0}",i, menu[i], menu[i].Price);
            }
            Console.WriteLine("Please, enter index of the car to evaluate it's price: ");
            int num;
            while (!Int32.TryParse(Console.ReadLine(), out num) | num>menu.Count | num<0)
            {
                Console.WriteLine("Wrong input!");
            }
            Console.CursorVisible = false;
            Console.Write("Evaluating");
            for(int i = 0; i< 5; i++)
            {
                Console.Write(".");
                Thread.Sleep(500);
            }
            Console.WriteLine();
            Console.CursorVisible = true;
            menu[num].EvaluateMyCar(menu[num].Price);
        }
    }
}
